package model.vo;

/**
 * Representation of a MovingViolation object
 */
public class VOMovingViolation implements Comparable<VOMovingViolation> {

	private int id;
	private String location;
	private int totalPaid;
	private String accidentIndicator;
	private String date;
	private String description;

	/**
	 * Metodo constructor
	 */
	public VOMovingViolation(int pOBJECTID, String pLOCATION, int pTOTALPAID, String pACCIDENTINDICATOR, String pTICKETISSUEDATE, String pVIOLATIONDESC){
		id = pOBJECTID;
		location = pLOCATION;
		totalPaid = pTOTALPAID;
		accidentIndicator = pACCIDENTINDICATOR;
		date = pTICKETISSUEDATE;
		description = pVIOLATIONDESC;
	}	
	
	/**
	 * @return id - Identificador unico de la infraccion
	 */
	public int objectId() {
		return id;
	}	
	
	
	/**
	 * @return location - Direccion en formato de texto.
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infraccion .
	 */
	public String getTicketIssueDate() {
		return date;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente paga el que recibio la infraccion en USD.
	 */
	public int getTotalPaid() {
		return totalPaid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		return accidentIndicator;
	}
		
	/**
	 * @return description - Descripcion textual de la infraccion.
	 */
	public String  getViolationDescription() {
		return description;
	}

	@Override
	public int compareTo(VOMovingViolation o) {
		return date.compareTo(o.getTicketIssueDate()) == 0? (id - o.objectId()):date.compareTo(o.getTicketIssueDate());
	}
	
	public String toString()
	{
		return id+","+location+","+totalPaid+","+accidentIndicator+","+date+","+description;
	}
}
