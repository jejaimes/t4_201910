package model.util;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import model.util.Sort;

public class SortTest {

	// Muestra de datos a ordenar
	private Comparable<muestra>[] datos;
	private muestra m;
	@Before
	public void setUp() throws Exception{

		datos=  new Comparable[1000];
		for (int i = 0; i < 1000; i++) {

			m= new muestra((1000-i));
			datos[i]= m;
		}


	}

	public void setup1()
	{
		datos=  new Comparable[10000];
		for (int i = 0; i < 10000; i++) {

			m= new muestra((10000-i));
			datos[i]= m;
		}
	}
	@Test
	public void testShellSort() {
		Sort.ordenarShellSort(datos);

		assertEquals(1, ((muestra)datos[0]).darValor());
		setup1();
		Sort.ordenarShellSort(datos);
		assertEquals(1, ((muestra)datos[0]).darValor());
	}

	@Test
	public void testMergeSort() {
		Sort.ordenarMergeSort(datos);

		assertEquals(1, ((muestra)datos[0]).darValor());
		setup1();
		Sort.ordenarShellSort(datos);
		assertEquals(1, ((muestra)datos[0]).darValor());
	}

	@Test
	public void testQuickSort() {
		Sort.ordenarQuickSort(datos);

		assertEquals(1, ((muestra)datos[0]).darValor());
		setup1();
		Sort.ordenarShellSort(datos);
		assertEquals(1, ((muestra)datos[0]).darValor());

	}

	class muestra implements Comparable<muestra>{

		int valor ;
		public muestra(int val) {
			valor= val;
		}

		int	darValor ()
		{
			return valor;
		}

		@Override
		public int compareTo(muestra o) {

			return  ((Integer)valor).compareTo(((Integer)o.darValor()))==0? 0 :((Integer)valor).compareTo(((Integer)o.darValor())) ;
		}
	}}
